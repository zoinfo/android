package com.example.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class AnimalActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animal);

        Intent intent2=getIntent();
        TextView textview2=(TextView)findViewById(R.id.textView10);
        String item=intent2.getStringExtra("id");

        textview2.setText(item);


        TextView textview3 = (TextView)findViewById(R.id.id1);

        TextView textview4 = (TextView)findViewById(R.id.id2);

        TextView textview5 = (TextView)findViewById(R.id.id3);

        TextView textview6 = (TextView)findViewById(R.id.id4);

        final TextView textview7 = (TextView)findViewById(R.id.id5);

        Button sauvegarder = findViewById(R.id.button);

        AnimalList liste = new AnimalList();

        String[] tab = AnimalList.getNameArray();

        for(int i=0;i<tab.length;i++) {

            if(item.equals(tab[i])){

                final Animal anm1 = liste.getAnimal(tab[i]);

                String esperance = anm1.getStrHightestLifespan();
                textview3.setText(esperance);

                String periode = anm1.getStrGestationPeriod();
                textview4.setText(periode);

                String poidn = anm1.getStrBirthWeight();
                textview5.setText(poidn);

                String poida = anm1.getStrAdultWeight();
                textview6.setText(poida);

                String status = anm1.getConservationStatus();
                textview7.setText(status);

                ImageView iv=(ImageView)findViewById(R.id.img);

                String photo = anm1.getImgFile();
                iv.setImageResource(getResources().getIdentifier(photo, "drawable", getPackageName()) );


                sauvegarder.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        anm1.setConservationStatus(textview7.getText().toString());
                        Toast.makeText(getApplication(),"Sauvegarde établie",Toast.LENGTH_LONG).show();

                    }


                });

            }

        }




















    }


}
